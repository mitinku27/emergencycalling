package com.user.emergencycallingapplication.StaticMode;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.user.emergencycallingapplication.R;
import com.user.emergencycallingapplication.SharefPref;
import com.user.emergencycallingapplication.SqlLiteDB.LoadData;
import com.user.emergencycallingapplication.SqlLiteDB.SqlLiteHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class OfflineHome extends AppCompatActivity {
    SharefPref sharefPref;
    SqlLiteHelper helper;
    RequestQueue queue;
    Spinner division;
    List<LoadData> police;
    List<LoadData> hospital;
    List<LoadData> ambulance;
    Toolbar toolbar;
    List<LoadData> fire;
    List<LoadData> rab;
    String divisionName;
    static int count=0;
    CardView policecard,rabcard,hospitlcard,firecard,ambulancecard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_home);

        division=findViewById(R.id.division);
        sharefPref=new SharefPref(this);

        final boolean network = isNetworkAvailable();
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("স্ট্যাটিক মোড");
        ambulancecard=findViewById(R.id.ambulance);
        policecard=findViewById(R.id.police);
        hospitlcard=findViewById(R.id.hospital);
        rabcard=findViewById(R.id.rab);
        firecard=findViewById(R.id.fire);



        /*int chk=sharefPref.getCheck();
        if (chk==0)
        {
            //data insertion will be execute in here
            helper= new SqlLiteHelper(this);
           // getAllData();
            getData();
            sharefPref.setCheck(1);
        }*/

        this.deleteDatabase("Emergency");
            helper= new SqlLiteHelper(this);
            getData();

        List<LoadData> dataList=new ArrayList<>();
        dataList.clear();

        dataList=helper.getAllDivision();

        final List<String> divName=new ArrayList<>();
        divName.clear();

        if (dataList!=null)
        {

            for (int i=0;i<dataList.size();i++)
            {
                LoadData data=dataList.get(i);
                divName.add(data.getName());
            }
            division.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, divName));

        }
        division.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                divisionName= divName.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ambulancecard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<LoadData> filterambulance=new ArrayList<>();
                filterambulance.clear();
                ambulance=helper.getAllAmbulance();
                for (int i=0;i<ambulance.size();i++)
                {

                    LoadData data=ambulance.get(i);
                    String test=data.getDivision();
                    if (data.getDivision().equals(divisionName))
                    filterambulance.add(data);
                }
                Intent i= new Intent(OfflineHome.this,DetailsViewer.class);
                i.putExtra("key", (Serializable) filterambulance);
                startActivity(i);

            }
        });
        hospitlcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<LoadData> filterambulance=new ArrayList<>();
                filterambulance.clear();
                hospital=helper.getAllHospital();
                for (int i=0;i<hospital.size();i++)
                {

                    LoadData data=hospital.get(i);
                    String test=data.getDivision();
                    if (data.getDivision().equals(divisionName))
                        filterambulance.add(data);
                }
                Intent i= new Intent(OfflineHome.this,DetailsViewer.class);
                i.putExtra("key", (Serializable) filterambulance);
                startActivity(i);

            }
        });
        policecard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<LoadData> filterambulance=new ArrayList<>();
                filterambulance.clear();
                ambulance=helper.getAllPolice();
                for (int i=0;i<ambulance.size();i++)
                {

                    LoadData data=ambulance.get(i);
                    String test=data.getDivision();
                    if (data.getDivision().equals(divisionName))
                        filterambulance.add(data);
                }
                Intent i= new Intent(OfflineHome.this,DetailsViewer.class);
                i.putExtra("key", (Serializable) filterambulance);
                startActivity(i);

            }
        });
        rabcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<LoadData> filterambulance=new ArrayList<>();
                filterambulance.clear();
                ambulance=helper.getAllRAB();
                for (int i=0;i<ambulance.size();i++)
                {

                    LoadData data=ambulance.get(i);
                    String test=data.getDivision();
                    if (data.getDivision().equals(divisionName))
                        filterambulance.add(data);
                }
                Intent i= new Intent(OfflineHome.this,DetailsViewer.class);
                i.putExtra("key", (Serializable) filterambulance);
                startActivity(i);

            }
        });
        firecard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<LoadData> filterambulance=new ArrayList<>();
                filterambulance.clear();
                ambulance=helper.getAllFire();
                for (int i=0;i<ambulance.size();i++)
                {

                    LoadData data=ambulance.get(i);
                    String test=data.getDivision();
                    if (data.getDivision().equals(divisionName))
                        filterambulance.add(data);
                }
                Intent i= new Intent(OfflineHome.this,DetailsViewer.class);
                i.putExtra("key", (Serializable) filterambulance);
                startActivity(i);

            }
        });










    }

    private void getData() {
        helper.insertDivisiion(1,"চট্টগ্রাম");
        helper.insertDivisiion(2,"ঢাকা");
        helper.insertDivisiion(3,"সিলেট");
        helper.insertDivisiion(4,"বরিশাল");
        helper.insertDivisiion(5,"রাজশাহী");
        helper.insertDivisiion(6,"খুলনা");
        helper.insertDivisiion(7,"রংপুর");
        helper.insertDivisiion(8,"ময়মনসিংহ");





        helper.insertPolice(1,"কোতয়ালি থানা","+01713373190","চট্টগ্রাম","ডিফল্ট");
        helper.insertPolice(2,"পাহাড়তলি থানা","+01713373257","চট্টগ্রাম","ডিফল্ট");
        helper.insertPolice(3,"পাঁচলাইশ থানা","+01713373258","চট্টগ্রাম","ডিফল্ট");
        helper.insertPolice(4,"চাঁদ্গাও থানা","+01713373259","চট্টগ্রাম","ডিফল্ট");
        helper.insertPolice(5,"খুলশি থানা","+01713373260","চট্টগ্রাম","ডিফল্ট");
        helper.insertPolice(6,"বাকলিয়া থানা","+01713373261","চট্টগ্রাম","ডিফল্ট");
        helper.insertPolice(7,"বায়েজিদ থানা","+01713373262","চট্টগ্রাম","ডিফল্ট");
        helper.insertPolice(8,"বন্দর থানা","+01713373267","চট্টগ্রাম","ডিফল্ট");
        helper.insertPolice(9,"ডাবল মুরিং থানা","+01713373268","চট্টগ্রাম","ডিফল্ট");
        helper.insertPolice(10,"হালিশহর থানা","+01713373269","চট্টগ্রাম","ডিফল্ট");


        helper.insertPolice(1,"রমনা থানা","+01713373125","ঢাকা","ডিফল্ট");
        helper.insertPolice(2,"ধানমন্ডি থানা","+01713373126","ঢাকা","ডিফল্ট");
        helper.insertPolice(3,"শাহবাগ থানা","+01713373127","ঢাকা","ডিফল্ট");
        helper.insertPolice(4,"নিউ মার্কেট থানা","+01713373128","ঢাকা","ডিফল্ট");
        helper.insertPolice(5,"লালবাগ থানা","+01713373134","ঢাকা","ডিফল্ট");

        helper.insertPolice(1,"খুলনা থানা","+01713373285","খুলনা","ডিফল্ট");
        helper.insertPolice(2,"সোনাডাঙা  থানা","+01713373286","খুলনা","ডিফল্ট");
        helper.insertPolice(3,"খালিসাপুর থানা","+01713373287","খুলনা","ডিফল্ট");
        helper.insertPolice(4,"দৌলতপুর থানা","+01713373286","খুলনা","ডিফল্ট");
        helper.insertPolice(5,"খান জাহান আলী থানা","+01713373285","খুলনা","ডিফল্ট");

        helper.insertPolice(1,"বোয়ালিয়া থানা","+01713373309","রাজশাহী","ডিফল্ট");
        helper.insertPolice(2,"রাজপাড়া থানা","+01713373310","রাজশাহী","ডিফল্ট");
        helper.insertPolice(3,"মতিহার থানা","+01713373311","রাজশাহী","ডিফল্ট");
        helper.insertPolice(4,"শাহ মাখদুম থানা","+01713373312","রাজশাহী","ডিফল্ট");

        helper.insertPolice(1,"বরিশাল কোতয়ালি থানা","+01713374267","বরিশাল","ডিফল্ট");
        helper.insertPolice(2,"হাজলা থানা","+01713374268","বরিশাল","ডিফল্ট");
        helper.insertPolice(3,"মেহেদিগঞ্জ থানা","+01713374269","বরিশাল","ডিফল্ট");
        helper.insertPolice(4,"মওলাদি থানা","+01713374270","বরিশাল","ডিফল্ট");


        helper.insertPolice(1,"কোতয়ালি সিলেট থানা","+01713374375","খুলনা","ডিফল্ট");
        helper.insertPolice(2,"বালাগঞ্জ  থানা","+01713374376","খুলনা","ডিফল্ট");
        helper.insertPolice(3,"জয়ন্তপুর থানা","+01713374377","খুলনা","ডিফল্ট");
        helper.insertPolice(4,"কো¤পানিগঞ্জ থানা","+01713374378","খুলনা","ডিফল্ট");

        helper.insertRAB(1,"test Rab1","+0154756638","chittagong","rangamati");
        helper.insertRAB(2,"test Rab2","+0154756638","chittagong","rangamati");
        helper.insertRAB(3,"test Rab3","+0154756638","chittagong","rangamati");
        helper.insertRAB(4,"test Rab4","+0154756638","chittagong","rangamati");
        helper.insertRAB(5,"test Rab5","+0154756638","chittagong","rangamati");
        helper.insertRAB(6,"test Rab6","+0154756638","chittagong","rangamati");


        helper.insertHospital(1,"test hospital1","+0154756638","chittagong","rangamati");
        helper.insertHospital(2,"test hospital2","+0154756638","chittagong","rangamati");
        helper.insertHospital(3,"test hospital3","+0154756638","chittagong","rangamati");
        helper.insertHospital(4,"test hospital4","+0154756638","chittagong","rangamati");
        helper.insertHospital(5,"test hospital5","+0154756638","chittagong","rangamati");
        helper.insertHospital(6,"test hospital6","+0154756638","chittagong","rangamati");


        helper.insertFireService(1,"test FireService1","+0154756638","chittagong","rangamati");
        helper.insertFireService(2,"test FireService2","+0154756638","chittagong","rangamati");
        helper.insertFireService(3,"test FireService3","+0154756638","chittagong","rangamati");
        helper.insertFireService(4,"test FireService4","+0154756638","chittagong","rangamati");
        helper.insertFireService(5,"test FireService5","+0154756638","chittagong","rangamati");
        helper.insertFireService(6,"test FireService6","+0154756638","chittagong","rangamati");

        helper.insertAmbulance(1,"বারডেম হসপিটল এম্বুলেন্স","58616641","ঢাকা","ঢাকা");
        helper.insertAmbulance(2,"গ্রিন এম্বুলেন্স ","+0154756638","ঢাকা","ঢাকা");
        helper.insertAmbulance(3,"আল দ্বীন এম্বুলেন্স","02-9362929","ঢাকা","ঢাকা");
        helper.insertAmbulance(4,"এপোলো হসপিটাল","+0154756638","ঢাকা","ঢাকা");
        helper.insertAmbulance(5,"খালেদ এ্যাম্বুলেন্স ","+1933246577","ঢাকা","ঢাকা");

        helper.insertAmbulance(1,"আলিফ এ্যাম্বুলেন্স সার্ভিস","01819325060","চট্টগ্রাম","চট্টগ্রাম");
        helper.insertAmbulance(2,"গডেডবডি ক্যারিয়ার","031-650000","চট্টগ্রাম","চট্টগ্রাম");
        helper.insertAmbulance(3,"জেনারেল হাসপাতাল","031-619584","চট্টগ্রাম","চট্টগ্রাম");
        helper.insertAmbulance(4,"হলি ক্রিসেন্ট হাসপাতাল ","031-620025","চট্টগ্রাম","চট্টগ্রাম");
        helper.insertAmbulance(5,"খমডার্ন হাসপাতাল","+01716074497","চট্টগ্রাম","চট্টগ্রাম");

        helper.insertAmbulance(1,"পাবনা মেডিক্যাল হাসপাতাল ","775606","রাজশাহী","রাজশাহী");
        helper.insertAmbulance(2,"পুলিশ হাসপাতাল এম্বুলেন্স","774308","রাজশাহী","রাজশাহী");
        helper.insertAmbulance(3,"সদর হাসপাতাল এম্বুলেন্স","775863","রাজশাহী","রাজশাহী");
        helper.insertAmbulance(4,"ইউভার্সিটি মেডিকেল এম্বুলেন্স","750169","রাজশাহী","রাজশাহী");
        helper.insertAmbulance(5,"ইসলামিক ব্যাংক হাসপাতাল সার্ভিস","++8801719978197","রাজশাহী","রাজশাহী");

        helper.insertAmbulance(1,"সিলেট পেশেন্ট কেয়ার এম্বুলেন্স","520585l","সিলেট","সিলেট");
        helper.insertAmbulance(2,"আল মদিনা হাসপাতাল"," 01711399742","সিলেট","সিলেট");
        helper.insertAmbulance(3,"সিলেট এম্বুলেন্স সার্ভিস","0821-721103","সিলেট","সিলেট");
        helper.insertAmbulance(4,"সিলেট সদর এম্বুলেন্স সার্ভিস","0821-713506","সিলেট","সিলেট");

        helper.insertAmbulance(1,"রংপুর মেডিক্যাল কলেজ","052163097","রংপুর","রংপুর");
        helper.insertAmbulance(2,"রংপুর পৌরসভা এম্বুলেন্স"," 01717974489","রংপুর","রংপুর");
        helper.insertAmbulance(3,"ফায়ার সার্ভিস এম্বুলেন্স","52165224","রংপুর","রংপুর");

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }




}
