package com.user.emergencycallingapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_main);
        setLocale("bn");
       /* recreate();*/



        int secondsDelayed = 2;
        new Handler().postDelayed(new Runnable() {
            public void run() {

                    startActivity(new Intent(MainActivity.this, ModuleSelector.class));

                finish();
            }
        }, secondsDelayed * 1000);



       /* apiInterface=ApiClient.getApiClient().create(ApiInterface.class);
        Call<List<AdminViewModel>> call=apiInterface.getAdminREport();
        call.enqueue(new Callback<List<AdminViewModel>>() {
            @Override
            public void onResponse(Call<List<AdminViewModel>> call, Response<List<AdminViewModel>> response) {
                mylist =response.body();
                AdminRecyclerAdapter adminRecyclerAdapter=new AdminRecyclerAdapter(getApplicationContext(), mylist);
                rev.setAdapter(adminRecyclerAdapter);
            }

            @Override
            public void onFailure(Call<List<AdminViewModel>> call, Throwable t) {

            }
        });*/


    }

    private void setLocale(String lang) {

        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = getSharedPreferences("Settings", Context.MODE_PRIVATE).edit();
        editor.putString("My_Language", lang);
        editor.apply();
    }

    public void loadLocale() {
        SharedPreferences preferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = preferences.getString("My_Language", "bn");
        setLocale(language);
    }
}
