package com.user.emergencycallingapplication;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharefPref {
    private static final String MY_PREFS_NAME = "checker";
    Context context;
    public SharefPref(Context context) {
        this.context=context;
    }
    public void setCheck(int chk)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putInt("idName", chk);
        editor.apply();
    }
   public int getCheck()
    {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

            int idName = prefs.getInt("idName", 0); //0 is the default value.

        return idName;
    }

}
