package com.user.emergencycallingapplication.OnlineMode.PoliceStation;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.util.Linkify;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.user.emergencycallingapplication.R;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter, GoogleMap.OnInfoWindowClickListener {

    private Activity context;
    String phone;

    public CustomInfoWindowAdapter(Activity context){
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = context.getLayoutInflater().inflate(R.layout.custominfowindow, null);

        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        TextView tvSubTitle = (TextView) view.findViewById(R.id.tv_subtitle);
        TextView phoneNumber=(TextView) view.findViewById(R.id.phoneNumber);
        TextView distance=(TextView) view.findViewById(R.id.distance);
        ImageView img=(ImageView) view.findViewById(R.id.img);
        String base=marker.getSnippet();
        if (!base.equals("0"))
        {
            String subtitle=base.substring(0,base.indexOf("@"));
             phone=base.substring(base.indexOf("@")+1,base.indexOf("#"));
            String dist=base.substring(base.indexOf("#")+1,base.length());
            tvSubTitle.setText(subtitle);
            tvSubTitle.setVisibility(View.GONE);

            distance.setText("দূরত্ব: "+dist+" কিমি.");

            if (!phone.startsWith("+"))
            {
                phoneNumber.setText("+88"+phone);
            }else {
                phoneNumber.setText(phone);
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context,"calling to phone",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                    context.startActivity(intent);
                }
            });

            Linkify.addLinks(phoneNumber,Linkify.PHONE_NUMBERS);
        }else {
            tvSubTitle.setVisibility(View.GONE);
            phoneNumber.setVisibility(View.GONE);
            distance.setVisibility(View.GONE);
            img.setVisibility(View.GONE);
        }

        tvTitle.setText(marker.getTitle());


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"calling to phone",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                context.startActivity(intent);
            }
        });

        return view;
    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        Toast.makeText(context,"calling to phone",Toast.LENGTH_LONG).show();
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        context.startActivity(intent);
    }
}
