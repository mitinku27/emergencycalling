package com.user.emergencycallingapplication.StaticMode;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.user.emergencycallingapplication.R;
import com.user.emergencycallingapplication.SqlLiteDB.LoadData;

import java.util.List;

public class DetailsViewer extends AppCompatActivity {
    RecyclerView dtr;
    Toolbar toolbar;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_viewer);
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("বিস্তারিত");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dtr= findViewById(R.id.dtr);
        dtr.setHasFixedSize(true);
        dtr.setLayoutManager(new LinearLayoutManager(this));
        List<LoadData> myList = (List<LoadData>) getIntent().getSerializableExtra("key");

        DetailsRecycler recycler=new DetailsRecycler(getApplicationContext(),myList);
        dtr.setAdapter(recycler);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
