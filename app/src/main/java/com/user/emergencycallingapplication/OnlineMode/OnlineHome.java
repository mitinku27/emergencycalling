package com.user.emergencycallingapplication.OnlineMode;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.user.emergencycallingapplication.OnlineMode.Ambulance.AmbulanceMap;
import com.user.emergencycallingapplication.OnlineMode.FireService.FireServiceMap;
import com.user.emergencycallingapplication.OnlineMode.Hospital.HospitalMap;
import com.user.emergencycallingapplication.OnlineMode.PoliceStation.PoliceStation;
import com.user.emergencycallingapplication.OnlineMode.RAB.RabMap;
import com.user.emergencycallingapplication.R;
import com.user.emergencycallingapplication.SqlLiteDB.SqlLiteHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OnlineHome extends AppCompatActivity {
    Spinner division,district;
    CardView police,rab,ambulance,hospital,fire;
    String divisionName,districtName;
    String divId;
    Toolbar toolbar;
    RequestQueue queue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_home);

        division=findViewById(R.id.division);
        district=findViewById(R.id.district);
        police=findViewById(R.id.police);
        rab=findViewById(R.id.rab);
        ambulance=findViewById(R.id.ambulance);
        hospital=findViewById(R.id.hospital);
        fire=findViewById(R.id.fire);

        police.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(OnlineHome.this, PoliceStation.class);
                intent.putExtra("put",divId);
                startActivity(intent);
            }
        });
        rab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(OnlineHome.this, RabMap.class);
                intent.putExtra("put",divId);
                startActivity(intent);

            }
        });
        ambulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(OnlineHome.this, AmbulanceMap.class);
                intent.putExtra("put",divId);
                startActivity(intent);

            }
        });
        hospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(OnlineHome.this, HospitalMap.class);
                intent.putExtra("put",divId);
                startActivity(intent);

            }
        });
        fire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= (new Intent(OnlineHome.this, FireServiceMap.class));
                intent.putExtra("put",divId);
                startActivity(intent);

            }
        });

        queue= Volley.newRequestQueue(this);
        String url = "http://emergencycalling.lazycrazycoder.com/get_divisions";
        final int[] idD=new int[12];
        final ArrayList<String> divName = new ArrayList<>();
        final SqlLiteHelper helper=new SqlLiteHelper(this);
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray array=response.getJSONArray("division");
                    try {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = (JSONObject) array.get(i);

                            divName.add(object.getString("name"));

                            idD[i] = object.getInt("id");

                            helper.insertDivisiion(object.getInt("id"),object.getString("name"));

                        }
                        division.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, divName));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(jsonArrayRequest);

        division.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                divisionName=divName.get(position);
                divId=String.valueOf(idD[position]);
                String url2="http://emergencycalling.lazycrazycoder.com/get_districts/"+idD[position];
                final ArrayList<String> disName = new ArrayList<>();
                final int[] id2=new int[100];
                JsonObjectRequest obj = new JsonObjectRequest(Request.Method.GET, url2, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            JSONArray array=response.getJSONArray("district");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = (JSONObject) array.get(i);

                                disName.add(object.getString("name"));

                                id2[i] = object.getInt("id");

                            }


                            district.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, disName));
                            district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    districtName=disName.get(position);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
                queue.add(obj);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
}
