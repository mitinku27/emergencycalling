package com.user.emergencycallingapplication.OnlineMode.Ambulance;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.user.emergencycallingapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class AmbulanceMap extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks
        , GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleMap mMap;
    GoogleApiClient client;
    LocationRequest request;
    String p;
    LatLng latLng;
    int check=0;
    ImageView back;
    FusedLocationProviderClient mFusedLocationClient;
    Location mylock;
    String did;
    RequestQueue queue;
    String url="http://emergencycalling.lazycrazycoder.com/get_ambulances";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Intent intent = getIntent();
        did = intent.getStringExtra("put");
        back=findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        client = new GoogleApiClient.Builder(this).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        client.connect();




        queue= Volley.newRequestQueue(this);
        JsonObjectRequest objectRequest= new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Double lat = null,lng=null;
                try {
                    JSONArray array=response.getJSONArray("ambulances");
                    for (int i=0;i<array.length();i++)
                    {
                        JSONObject jsonObject= (JSONObject) array.get(i);
                        String title=jsonObject.getString("name");
                        String id=jsonObject.getString("id");
                        String phone=jsonObject.getString("phone");
                        String Latitude=jsonObject.getString("latitude");
                        String Longitude=jsonObject.getString("longitude");

                        String division_id=jsonObject.getString("division_id");
                        String District_id=jsonObject.getString("district_id");

                        if (division_id.equals(did)&&Latitude!=null&&Longitude!=null)
                        {
                            lat=Double.valueOf(Latitude);
                            lng=Double.valueOf(Longitude);
                            CameraUpdate center =
                                    CameraUpdateFactory.newLatLng(new LatLng(lat,lng));
                            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
                            mMap.moveCamera(center);
                            mMap.animateCamera(zoom);
                            MarkerOptions markerOptions = new MarkerOptions();
                            Location destination = new Location("");
                            destination.setLatitude(Double.valueOf(Latitude));
                            destination.setLongitude(Double.valueOf(Longitude));
                            markerOptions.title(title);
                            double d=mylock.distanceTo(destination)/1000;
                            String k=(new DecimalFormat("##.##").format(d));
                            Drawable drawable=getResources().getDrawable(R.drawable.ic_policeman);
                            // double d=2.0;
                            String description="na";
                            markerOptions.icon(getMarkerIconFromDrawable(drawable));
                            markerOptions.snippet(description+"@"+phone+"#"+k);
                            markerOptions.position(new LatLng(Double.valueOf(Latitude),Double.valueOf(Longitude)));
                            mMap.addMarker(markerOptions);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(objectRequest);


        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                if (!marker.getSnippet().equals("0"))
                {
                    Toast.makeText(getApplicationContext(),"calling to phone",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + p));
                    getApplication().startActivity(intent);
                }

            }
        });

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        request = new LocationRequest().create();

        request.setFastestInterval(10000);
        request.setInterval(15000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        //  LocationServices.FusedLocationApi.requestLocationUpdates(client, request,  this);




        mFusedLocationClient=LocationServices.getFusedLocationProviderClient(getApplicationContext());
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations, this can be null.
                        if (location != null) {
                            mylock=location;
                            CameraUpdate center =
                                    CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),
                                            location.getLongitude()));
                            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
                            mMap.moveCamera(center);
                            mMap.animateCamera(zoom);

                            String station_name = "This is Title";
                            String phone = "4365234";
                            String description="very cool police station";
                            String snipet=phone+"#"+description;
                            latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            if (check==0)
                            {
                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.title("Marker at Home");
                                // markerOptions.icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_policeman));
                                //    BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_boy);
                                // markerOptions.icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_boy));
                                //    markerOptions.icon(icon);
                                Drawable drawable=getResources().getDrawable(R.drawable.ic_boy);
                                markerOptions.icon(getMarkerIconFromDrawable(drawable));

                                markerOptions.snippet("0");
                                markerOptions.position(latLng);
                                mMap.addMarker(markerOptions);
                                check++;
                            }

                            AmbulanceCustomInfoWindowAdapter adapter = new AmbulanceCustomInfoWindowAdapter(AmbulanceMap.this);
                            mMap.setInfoWindowAdapter(adapter);
                        }
                    }
                });



        Log.d("hello", "hello");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location == null) {
            Toast.makeText(this, "location not found", Toast.LENGTH_LONG).show();
        } else {
            //নিজের অবস্থানে মার্কার শো করা
            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),
                            location.getLongitude()));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
            mMap.moveCamera(center);
            mMap.animateCamera(zoom);

            mylock=location;
            latLng = new LatLng(location.getLatitude(), location.getLongitude());
            if (check==0)
            {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.title("Marker at Home");

                Drawable drawable=getResources().getDrawable(R.drawable.ic_boy);
                markerOptions.icon(getMarkerIconFromDrawable(drawable));
                markerOptions.snippet("0");
                markerOptions.position(latLng);
                mMap.addMarker(markerOptions);
                check++;
            }

            AmbulanceCustomInfoWindowAdapter adapter = new AmbulanceCustomInfoWindowAdapter(AmbulanceMap.this);
            mMap.setInfoWindowAdapter(adapter);

        }
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
