package com.user.emergencycallingapplication.StaticMode;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.user.emergencycallingapplication.R;
import com.user.emergencycallingapplication.SqlLiteDB.LoadData;

import java.util.List;

public class DetailsRecycler extends RecyclerView.Adapter<DetailsRecycler.ViewHolder> {

    Context context;
    List<LoadData> dataList;

    public DetailsRecycler(Context context, List<LoadData> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_details_view,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailsRecycler.ViewHolder viewHolder, int i) {
        final LoadData data=dataList.get(i);
        viewHolder.name.setText(data.getName());
        viewHolder.phone.setText(data.getphone());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"calling to phone",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + data.getphone()));
                context.startActivity(intent);
            }
        });


    }



    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,phone;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            phone=itemView.findViewById(R.id.phone);
        }
    }
}
