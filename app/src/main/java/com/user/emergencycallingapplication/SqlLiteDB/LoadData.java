package com.user.emergencycallingapplication.SqlLiteDB;

import java.io.Serializable;

public class LoadData implements Serializable {

    public static final String POLICE = "police";
    public static final String AMBULANCE = "ambulance";
    public static final String RAB = "rab";
    public static final String HOSPITAL = "hospital";
    public static final String FIRESERVICE = "fire";


    public static final String DIVISION = "division";
    public static final String DISTRICT = "district";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_phone = "phone";
    public static final String COLUMN_division = "division";
    public static final String COLUMN_district = "district";

    private int id;
    private String name;
    private String phone;
    private String division;
    private String district;


    // Create table SQL query
    public static final String CREATE_TABLE_POLICE =
            "CREATE TABLE " + POLICE + "("
                    + COLUMN_ID + " INTEGER,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_division + " TEXT,"
                    + COLUMN_district + " TEXT,"
                    + COLUMN_phone + " TEXT"
                    + ")";
    public static final String CREATE_TABLE_AMBULANCE =
            "CREATE TABLE " + AMBULANCE + "("
                    + COLUMN_ID + " INTEGER,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_division + " TEXT,"
                    + COLUMN_district + " TEXT,"
                    + COLUMN_phone + " TEXT"
                    + ")";
    public static final String CREATE_TABLE_HOSPITAL =
            "CREATE TABLE " + HOSPITAL + "("
                    + COLUMN_ID + " INTEGER,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_division + " TEXT,"
                    + COLUMN_district + " TEXT,"
                    + COLUMN_phone + " TEXT"
                    + ")";
    public static final String CREATE_TABLE_RAB =
            "CREATE TABLE " + RAB + "("
                    + COLUMN_ID + " INTEGER,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_division + " TEXT,"
                    + COLUMN_district + " TEXT,"
                    + COLUMN_phone + " TEXT"
                    + ")";
    public static final String CREATE_TABLE_FIRESERVICE =
            "CREATE TABLE " + FIRESERVICE + "("
                    + COLUMN_ID + " INTEGER,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_division + " TEXT,"
                    + COLUMN_district + " TEXT,"
                    + COLUMN_phone + " TEXT"
                    + ")";
    public static final String CREATE_TABLE_DIVISION =
            "CREATE TABLE " + DIVISION + "("
                    + COLUMN_ID + " INTEGER,"
                    + COLUMN_NAME + " TEXT"
                    + ")";
    public static final String CREATE_TABLE_DISTRICT =
            "CREATE TABLE " + DISTRICT + "("
                    + COLUMN_ID + " INTEGER,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_division + " TEXT"
                    + ")";

    public LoadData() {
    }

    public LoadData(int id, String name, String phone) {
        this.id = id;
        this.name = name;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getphone() {
        return phone;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
