package com.user.emergencycallingapplication.SqlLiteDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;


public class SqlLiteHelper extends SQLiteOpenHelper {
    private static final String DBname="Emergency";
    private static final int DBversion=1;

    public SqlLiteHelper( Context context) {
        super(context, DBname, null, DBversion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LoadData.CREATE_TABLE_POLICE);
        db.execSQL(LoadData.CREATE_TABLE_HOSPITAL);
        db.execSQL(LoadData.CREATE_TABLE_AMBULANCE);
        db.execSQL(LoadData.CREATE_TABLE_RAB);
        db.execSQL(LoadData.CREATE_TABLE_FIRESERVICE);
        db.execSQL(LoadData.CREATE_TABLE_DIVISION);
        db.execSQL(LoadData.CREATE_TABLE_DISTRICT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + LoadData.POLICE);
        db.execSQL("DROP TABLE IF EXISTS " + LoadData.RAB);
        db.execSQL("DROP TABLE IF EXISTS " + LoadData.HOSPITAL);
        db.execSQL("DROP TABLE IF EXISTS " + LoadData.AMBULANCE);
        db.execSQL("DROP TABLE IF EXISTS " + LoadData.FIRESERVICE);
        db.execSQL("DROP TABLE IF EXISTS " + LoadData.DIVISION);
        db.execSQL("DROP TABLE IF EXISTS " + LoadData.DISTRICT);


    }





    public long insertPolice(int id,String name,String phone,String division,String district) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(LoadData.COLUMN_ID, id);
        values.put(LoadData.COLUMN_NAME, name);
        values.put(LoadData.COLUMN_phone, phone);
        values.put(LoadData.COLUMN_division, division);
        values.put(LoadData.COLUMN_district, district);

        // insert row
        long test = db.insert(LoadData.POLICE, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }


    public long insertHospital(int id,String name,String phone,String division,String district) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(LoadData.COLUMN_ID, id);
        values.put(LoadData.COLUMN_NAME, name);
        values.put(LoadData.COLUMN_phone, phone);
        values.put(LoadData.COLUMN_division, division);
        values.put(LoadData.COLUMN_district, district);

        // insert row
        long test = db.insert(LoadData.HOSPITAL, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }
    public long insertRAB(int id,String name,String phone,String division,String district) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(LoadData.COLUMN_ID, id);
        values.put(LoadData.COLUMN_NAME, name);
        values.put(LoadData.COLUMN_phone, phone);
        values.put(LoadData.COLUMN_division, division);
        values.put(LoadData.COLUMN_district, district);

        // insert row
        long test = db.insert(LoadData.RAB, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }
    public long insertAmbulance(int id,String name,String phone,String division,String district) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(LoadData.COLUMN_ID, id);
        values.put(LoadData.COLUMN_NAME, name);
        values.put(LoadData.COLUMN_phone, phone);
        values.put(LoadData.COLUMN_division, division);
        values.put(LoadData.COLUMN_district, district);

        // insert row
        long test = db.insert(LoadData.AMBULANCE, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }
    public long insertFireService(int id,String name,String phone,String division,String district) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(LoadData.COLUMN_ID, id);
        values.put(LoadData.COLUMN_NAME, name);
        values.put(LoadData.COLUMN_phone, phone);
        values.put(LoadData.COLUMN_division, division);
        values.put(LoadData.COLUMN_district, district);

        // insert row
        long test = db.insert(LoadData.FIRESERVICE, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }
    public long insertDivisiion(int id,String name) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(LoadData.COLUMN_ID, id);
        values.put(LoadData.COLUMN_NAME, name);


        // insert row
        long test = db.insert(LoadData.DIVISION, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }
    public long insertDistrict(int id,String name,String division) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(LoadData.COLUMN_ID, id);
        values.put(LoadData.COLUMN_NAME, name);
        values.put(LoadData.COLUMN_division, division);

        // insert row
        long test = db.insert(LoadData.DISTRICT, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }


    public List<LoadData> getAllAmbulance() {
        List<LoadData> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + LoadData.AMBULANCE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                LoadData data= new LoadData();
                data.setId(cursor.getInt(cursor.getColumnIndex(LoadData.COLUMN_ID)));
                data.setName(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_NAME)));
                data.setPhone(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_phone)));
                data.setDivision(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_division)));
                data.setDistrict(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_district)));

                notes.add(data);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }
    public List<LoadData> getAllRAB() {
        List<LoadData> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + LoadData.RAB;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                LoadData data= new LoadData();
                data.setId(cursor.getInt(cursor.getColumnIndex(LoadData.COLUMN_ID)));
                data.setName(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_NAME)));
                data.setPhone(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_phone)));
                data.setDivision(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_division)));
                data.setDistrict(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_district)));

                notes.add(data);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }
    public List<LoadData> getAllFire() {
        List<LoadData> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + LoadData.FIRESERVICE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                LoadData data= new LoadData();
                data.setId(cursor.getInt(cursor.getColumnIndex(LoadData.COLUMN_ID)));
                data.setName(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_NAME)));
                data.setPhone(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_phone)));
                data.setDivision(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_division)));
                data.setDistrict(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_district)));

                notes.add(data);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }
    public List<LoadData> getAllPolice() {
        List<LoadData> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + LoadData.POLICE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                LoadData data= new LoadData();
                data.setId(cursor.getInt(cursor.getColumnIndex(LoadData.COLUMN_ID)));
                data.setName(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_NAME)));
                data.setPhone(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_phone)));
                data.setDivision(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_division)));
                data.setDistrict(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_district)));

                notes.add(data);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }
    public List<LoadData> getAllHospital() {
        List<LoadData> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + LoadData.HOSPITAL;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                LoadData data= new LoadData();
                data.setId(cursor.getInt(cursor.getColumnIndex(LoadData.COLUMN_ID)));
                data.setName(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_NAME)));
                data.setPhone(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_phone)));
                data.setDivision(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_division)));
                data.setDistrict(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_district)));

                notes.add(data);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }
    public List<LoadData> getAllDivision() {
        List<LoadData> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + LoadData.DIVISION;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                LoadData data= new LoadData();
                data.setId(cursor.getInt(cursor.getColumnIndex(LoadData.COLUMN_ID)));
                data.setName(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_NAME)));

                notes.add(data);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }

    public List<LoadData> getAllDistrict() {
        List<LoadData> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + LoadData.DISTRICT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                LoadData data= new LoadData();
                data.setId(cursor.getInt(cursor.getColumnIndex(LoadData.COLUMN_ID)));
                data.setName(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_NAME)));
                data.setDivision(cursor.getString(cursor.getColumnIndex(LoadData.COLUMN_division)));


                notes.add(data);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }






}
